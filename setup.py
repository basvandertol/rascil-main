#!/usr/bin/env python
# -*- coding: utf-8 -*-

import glob
import os
import sys
from distutils.sysconfig import get_config_vars

from setuptools import setup

# Bail on Python < 3
assert sys.version_info[0] >= 3

with open("README.md") as readme_file:
    readme = readme_file.read()

version = {}
VERSION_PATH = os.path.join("rascil", "version.py")
with open(VERSION_PATH, "r") as file:
    exec(file.read(), version)

optional_dependencies = {
    "notebooks": [
        "jupyter",
        # TODO: remove jupyter-client once the tornado incompatibility with
        #  distributed is resolved. It is not a req for rascil, so can be safely removed
        "jupyter-client < 7.3.5",
    ],
    "radler": ["radler"],  # used via ska-sdp-func-python
    "aoflagger": ["aoflagger"],
    "gpu": ["cupy"],  # used via ska-sdp-func-python
    "PyBDSF": ["bdsf"],
}

direct_dependencies = [
    "astropy",
    "dask",
    "dask-memusage",
    "distributed",
    "h5py",
    "matplotlib",
    "numpy",
    "pandas",
    "python-casacore",
    "reproject",
    "scipy",
    "seqfile",
    "ska-sdp-datamodels",
    "ska-sdp-func-python",
    "tabulate",
    "xarray",
    "ska-sdp-func",
]


def fix_req_versions():
    """
    Get RASCIL's direct dependencies from requirements.txt.
    Versions are pinned between current minor in requirements.txt (>=)
    and the next minor version (<).

    requirements.txt is regularly updated from requirements.in.
    This way we make sure these and setup.py are consistent.
    """
    with open("requirements.txt", "r") as req_file:
        lines = req_file.readlines()
        new_req = []
        for req in reversed(direct_dependencies):
            for line in reversed(lines):
                if not line.strip().startswith("#") and (
                    f"{req}==" in line or f"{req} @" in line or f"{req}[" in line
                ):
                    if "@" in line:
                        # append packages installed from git without modification
                        new_req.append(line.strip())
                        continue
                    req_data = line.strip().split("==")
                    version = req_data[-1].split(".")
                    old_minor = f"{version[0]}.{version[1]}"
                    new_minor = f"{version[0]}.{int(version[1])+1}"
                    final_req = f"{req_data[0]} >= {old_minor}, < {new_minor}"
                    new_req.append(final_req)
                del line

    return new_req


requirements = fix_req_versions()

# MF. This is a workaround to be able to build the library with MacOS
if sys.platform == "darwin":
    vars = get_config_vars()
    vars["LDSHARED"] = vars["LDSHARED"].replace("-bundle", "-dynamiclib")
    os.environ["CC"] = "clang"

# NB. These are not really Python extensions (i.e., they do not
# Py_Initialize() and they do define main() ), we are just cheating to
# re-use the setuptools build support.

packages = ["rascil"]
packages_data = [
    i
    for p in packages
    for i in glob.glob(p + "/*/")
    + glob.glob(p + "/*/*/")
    + glob.glob(p + "/*/*/*/")
    + glob.glob(p + "/*/*/*/*/")
    + glob.glob(p + "/*/*/*/*/")
]

setup(
    name="rascil",
    version=version["__version__"],
    python_requires=">=3.9, <3.11",
    description="Radio Astronomy Simulation, Calibration, and Imaging Library",
    long_description=readme + "\n\n",
    author="See CONTRIBUTORS",
    url="https://gitlab.com/ska-telescope/external/rascil-main",
    license="Apache License Version 2.0",
    zip_safe=False,
    classifiers=[
        "Development Status :: Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: BSD License",
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
    ],
    packages=(packages + packages_data),
    install_requires=requirements,
    extra_requires=optional_dependencies,
    scripts=["bin/get_rascil_data"],
    test_suite="tests",
    tests_require=["pytest"],
)
