.. _rascil_data:

.. toctree::
   :maxdepth: 2

Data containers used by RASCIL
==============================

RASCIL holds data in python Classes. The bulk data and attributes are usually kept in a xarray.Dataset.
For each xarray based class there is an accessor which holds class specific methods and properties.

Note that the data models have been migrated into
the `SKA SDP Python-based Data Models <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels.git>`_ directory.
Please refer to the documentation there for more information.