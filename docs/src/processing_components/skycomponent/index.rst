.. _rascil_processing_components_skycomponent:

.. py:currentmodule:: rascil.processing_components.skycomponent

Sky components
**************


.. automodapi::    rascil.processing_components.skycomponent.plot_skycomponent
   :no-inheritance-diagram:
