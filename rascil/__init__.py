from . import phyconst
from . import processing_components
from . import workflows
from .processing_components.util.installation_checks import check_data_directory

check_data_directory(fatal=False)
